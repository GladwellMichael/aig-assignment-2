Maximum = 100; 
Minimum = -100;

function minimax(depth, nodeIndex, maxPlayer, values, alpha, beta)
    
    if depth == 3
        return values[nodeIndex] 
    end

    if maxPlayer
        best = Minimum

        for i in 1:3
            val = minimax(depth + 1, nodeIndex + 1, false, values, alpha, beta)
    
            best = max(best, val) 
            alpha = max(alpha, best)

            if beta <= alpha 
                break
            end
        return best
        end
         
    else 
        best = Maximum

        for i in 1:3
            val = minimax(depth + 1, nodeIndex + 1, true, values, alpha, beta) 
            best = min(best, val) 
            beta = min(beta, best)

            if beta <= alpha
                break
            end
        return best
        end

    end
end                     

nodeValues = [31, 54, 26, 19, 31, 2, 0, -12, 81, 6, 0, 33, 61, 1, 7 ,4, 3, 10, 43, 3, 11, 21, 0, -9, 7, 2, 66, 7, 6, 11, 12 ,1]
println("The Values are: ", nodeValues)
println("The optimal value is :", minimax(1, 1, true, nodeValues, Minimum, Maximum)) 
