using  Random

# initialising the knapsack with random binary values
function initialise(length)
    knapsack = rand([0,1],1,length)
    return knapsack  
end


# Mutate fuction to swap the binary values in the 
# knapsack
function Mutate(knapsack)
    toSwap = rand(1:length(knapsack))

    if(knapsack[toSwap] == 0)
        knapsack[toSwap] = 1
    else
        knapsack[toSwap] = 0
    end
    return  knapsack
end

# evaluate function to evaluate the collection if 
# is meeting the requirements
function evaluate(knapsack, weights ,values ,capacity)
    total_weight = 0
    total_values = 0
    #evaluatinng the values that are supposed
    # to encluded into the collection
    for i in 1:(length(knapsack))
        #Calculating all the values ones in the knapsack
        if(knapsack[i] == 1)
            total_weight = total_weight + weights[i]
            total_values = total_values + values[i]
        end
    end
    #checking to see if the current total weight of collections
    # is not exceeding the expected capacity
    if(total_weight > capacity)
        return 0
        else
            return total_values
    end 
end


#Hill climbing function
function hill_climbing(n_of_items, weights ,values ,capacity)
   
    #Calling initialise and evaluate functions
    best_node = initialise(n_of_items)
    best_fit = evaluate(best_node , weights ,values , capacity)
    
    #climbing the hill from node to node
    for _ in 1:n_of_items
       
        current_node = Mutate(best_node)
        print("\nCurrent Node  :", current_node)
        candidate_fit = evaluate(current_node , weights ,values , capacity)
        print("\nCandidate fit :",candidate_fit)
        print("\n")
        
        #Evaluatimg the total value
        if(candidate_fit > best_fit)
            best_fit = candidate_fit
            best_node = current_node
    
        end
        
    end
    #returning the total value of the collection
    return best_fit
end

#The  auguments to pass into hill climbing function
values = [50 ,100 ,150 ,200]
weights = [8 , 16 , 32 , 40]
number_of_items = 4
capacity = 64

#invoking the hill climbing fuctionn
results = hill_climbing(number_of_items ,weights ,values ,capacity)
print("\n#################################")
print("\n Total value :",results)
print("\n#################################")
print("\n")

